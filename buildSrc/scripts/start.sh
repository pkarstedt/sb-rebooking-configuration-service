#!/bin/bash

application_name="$1"
application_version="$2"
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

get_instance_id() {
    echo $(get_metadata "instance-rbConfigId" "local")
}

get_instance_ip() {
    echo $(get_metadata "local-ipv4" "127.0.0.1")
}

get_metadata() {
    local type="$1"
    local default="$2"
    local url="http://169.254.169.254/latest/meta-data/$type"
    local result=$(curl --connect-timeout 5 --max-time 5 -s "$url")
    if [[ "$result" == "null" ]] || [[ "$result" == "" ]];
    then
        result="$default"
    fi
    echo "$result"
}

start_ntp() {
    ntpd -c /etc/ntp.conf
}

export AWS_INSTANCE_ID="$(get_instance_id)"
echo "AWS_INSTANCE_ID=${AWS_INSTANCE_ID}"

export AWS_INSTANCE_IP="$(get_instance_ip)"
echo "AWS_INSTANCE_IP=${AWS_INSTANCE_IP}"

export SPRING_BOOT_APPLICATION_NAME="$application_name"
echo "SPRING_BOOT_APPLICATION_NAME=${SPRING_BOOT_APPLICATION_NAME}"

export SPRING_BOOT_APPLICATION_VERSION="$application_version"
echo "SPRING_BOOT_APPLICATION_VERSION=${SPRING_BOOT_APPLICATION_VERSION}"

if [ -f "$script_dir/additional.sh" ]; then
    source "$script_dir/additional.sh"
fi

source "$script_dir/$application_name"

start_ntp