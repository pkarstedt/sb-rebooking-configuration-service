package de.hrs.sb.rebooking.configuration.service.application.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import de.hrs.sb.rebooking.configuration.service.model.ProcIndicators;
import de.hrs.sb.rebooking.configuration.service.application.exception.DaoException;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DdbRbConfigDao implements RbConfigDao {
    private DynamoDBMapperConfig dynamoDBMapperConfig;
    private DynamoDBMapper mapper;

    @Autowired
    public DdbRbConfigDao(DynamoDBMapperConfig dynamoDBMapperConfig, DynamoDBMapper mapper) {
        this.dynamoDBMapperConfig = dynamoDBMapperConfig;
        this.mapper = mapper;
    }

    public String createRbConfig(final RbConfig config) throws DaoException {
        mapper.save(config, dynamoDBMapperConfig);
        return config.getRbConfigId();
    }

    public List<RbConfig> getIdle() {

        final Map<String, AttributeValue> attrVal = new HashMap<>();
        attrVal.put(":p1", new AttributeValue().withS(ProcIndicators.IDLE));

        final DynamoDBScanExpression expr = new DynamoDBScanExpression()
                .withFilterExpression("procIndicator = :p1")
                .withExpressionAttributeValues(attrVal);

        return mapper.scan(RbConfig.class, expr, dynamoDBMapperConfig);
    }

    public void save(final RbConfig rbConfig) {
        mapper.save(rbConfig, dynamoDBMapperConfig);
    }

}
