package de.hrs.sb.rebooking.configuration.service.application;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.OutputStream;

public class Utils {

    public void writeToStream(String data, OutputStream output) throws IOException {
        IOUtils.write(data, output);
    }
}
