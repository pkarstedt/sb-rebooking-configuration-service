package de.hrs.sb.rebooking.configuration.service.application.exception;

public class DaoException extends Exception {
    public DaoException(final String msg, final Exception exception) {
        super(msg, exception);
    }

    public DaoException(final String msg) {
        super(msg);
    }
}
