package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.hrs.sb.rebooking.configuration.service.application.configuration.ExceptionMessages;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.model.SaveRbConfigRequest;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class SaveRbConfigApiAction extends AbstractApiAction {

    @Autowired
    public SaveRbConfigApiAction(ObjectMapper objectMapper, RbConfigDao dao) {
        super(objectMapper, dao);
    }

    public String handle(final String request, final Context lambdaContext)
        throws BadRequestException, InternalErrorException, JsonProcessingException {

        logger = lambdaContext.getLogger();
        final ObjectReader reader = objectMapper.readerFor(SaveRbConfigRequest.class);

        SaveRbConfigRequest saveRequest;
        try {
            saveRequest = reader.readValue(request);
        } catch (IOException e) {
            logger.log("Could not read from request");
            throw new InternalErrorException("Could not read from request", e);
        }

        if (saveRequest == null || saveRequest.getPayload().getRbConfigId().trim().equals("")) {
            logger.log(ExceptionMessages.EX_INVALID_INPUT);
            throw new BadRequestException(ExceptionMessages.EX_INVALID_INPUT);
        }

        dao.save(saveRequest.getPayload());

        return objectMapper.writeValueAsString(saveRequest.getPayload());
    }
}
