package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractApiAction implements ApiAction {

    protected transient LambdaLogger logger;
    protected ObjectMapper objectMapper;
    protected RbConfigDao dao;

    @Autowired
    public AbstractApiAction(ObjectMapper objectMapper, RbConfigDao dao) {
        this.objectMapper = objectMapper;
        this.dao = dao;
    }
}
