package de.hrs.sb.rebooking.configuration.service.application.exception;

public class InternalErrorException extends Exception {
    private static final String PREFIX = "INT_ERROR: ";


    public InternalErrorException(final String msg, final Exception exception) {
        super(PREFIX + msg, exception);
    }

    public InternalErrorException(final String msg) {
        super(PREFIX + msg);
    }
}
