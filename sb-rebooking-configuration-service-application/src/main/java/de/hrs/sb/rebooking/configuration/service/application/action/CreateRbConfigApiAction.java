package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.hrs.sb.rebooking.configuration.service.application.configuration.ExceptionMessages;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.DaoException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import de.hrs.sb.rebooking.configuration.service.model.CreateRbConfigRequest;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;

import java.io.IOException;

public class CreateRbConfigApiAction extends AbstractApiAction {

    public CreateRbConfigApiAction(ObjectMapper objectMapper, RbConfigDao dao) {
        super(objectMapper, dao);
    }

    public String handle(final String request, final Context lambdaContext)
            throws BadRequestException, InternalErrorException, JsonProcessingException {

        logger = lambdaContext.getLogger();
        final ObjectReader reader = objectMapper.readerFor(CreateRbConfigRequest.class);

        CreateRbConfigRequest createRbConfigRequest;
        try {
            createRbConfigRequest = reader.readValue(request);
        } catch (IOException e) {
            throw new InternalErrorException("Could not read from request", e);
        }

        if (createRbConfigRequest == null || createRbConfigRequest.getPayload().getReservationId().trim().equals("")) {
            throw new BadRequestException(ExceptionMessages.EX_INVALID_INPUT);
        }

        RbConfig rbConfig = createRbConfigRequest.getPayload();

        String rbConfigId;

        try {
            rbConfigId = dao.createRbConfig(rbConfig);
        } catch (DaoException e) {
            logger.log("Error while creating new rbconfig\n" + e.getMessage());
            throw new InternalErrorException(ExceptionMessages.EX_DAO_ERROR, e);
        }

        if (rbConfigId == null || rbConfigId.trim().equals("")) {
            logger.log("RbConfig ID is null or empty");
            throw new InternalErrorException(ExceptionMessages.EX_DAO_ERROR);
        }

        rbConfig.setRbConfigId(rbConfigId);

        return objectMapper.writeValueAsString(rbConfig);
    }
}
