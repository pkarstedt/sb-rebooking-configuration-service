package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.hrs.sb.rebooking.configuration.service.application.configuration.ExceptionMessages;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import de.hrs.sb.rebooking.configuration.service.model.FindIdleRequest;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;

import java.io.IOException;
import java.util.List;

public class FindIdleApiAction extends AbstractApiAction {

    public FindIdleApiAction(ObjectMapper objectMapper, RbConfigDao dao) {
        super(objectMapper, dao);
    }

    public String handle(final String request, final Context context)
            throws InternalErrorException, BadRequestException, JsonProcessingException {

        final ObjectReader reader = objectMapper.readerFor(FindIdleRequest.class);

        FindIdleRequest findIdleRequest;
        try {
            findIdleRequest = reader.readValue(request);
        } catch (IOException e) {
            throw new InternalErrorException("Could not read from request", e);
        }

        if(findIdleRequest == null || findIdleRequest.getPayload().getProcIndicator().trim().equals("")) {
            throw new BadRequestException(ExceptionMessages.EX_INVALID_INPUT);
        }

        List<RbConfig> configs;
        try {
            configs = dao.getIdle();
        } catch (Exception e) {
            throw new InternalErrorException("Could not read from DynamoDB", e);
        }

        return objectMapper.writeValueAsString(dao.getIdle());
    }
}
