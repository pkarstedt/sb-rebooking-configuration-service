package de.hrs.sb.rebooking.configuration.service.application;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.hrs.sb.rebooking.configuration.service.application.configuration.ApplicationConfiguration;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.InputStream;
import java.io.OutputStream;

public class Application {

    public static void handle(final InputStream request, final OutputStream response, final Context context)
            throws BadRequestException, InternalErrorException, JsonProcessingException {

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(ApplicationConfiguration.class);
        ctx.refresh();

        RequestRouter requestRouter = ctx.getBean(RequestRouter.class);
        requestRouter.setCtx(ctx);

        requestRouter.lambdaHandler(request, response, context);
    }
}
