package de.hrs.sb.rebooking.configuration.service.application.exception;

public class BadRequestException extends Exception {
    private static final String PREFIX = "BAD_REQ: ";

    public BadRequestException(final String msg, final Exception exception) {
        super(PREFIX + msg, exception);
    }

    public BadRequestException(final String msg) {
        super(PREFIX + msg);
    }
}
