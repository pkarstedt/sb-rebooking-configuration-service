package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;

public interface ApiAction {
    String handle(String request, Context lambdaContext)
            throws BadRequestException, InternalErrorException, JsonProcessingException;
}
