package de.hrs.sb.rebooking.configuration.service.application.model;

import de.hrs.sb.rebooking.configuration.service.application.exception.DaoException;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;

import java.util.List;

public interface  RbConfigDao {

    String createRbConfig(RbConfig config) throws DaoException;

    List<RbConfig> getIdle();

    void save(RbConfig config);
}
