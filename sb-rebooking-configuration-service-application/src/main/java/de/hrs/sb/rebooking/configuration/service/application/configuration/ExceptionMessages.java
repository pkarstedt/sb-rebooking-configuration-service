package de.hrs.sb.rebooking.configuration.service.application.configuration;

public class ExceptionMessages {
    public static final String EX_INVALID_INPUT = "Invalid input parameters";
    public static final String EX_DAO_ERROR = "Error loading user";
}
