package de.hrs.sb.rebooking.configuration.service.application;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hrs.sb.rebooking.configuration.service.application.action.ApiAction;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import org.apache.commons.io.IOUtils;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class RequestRouter {

    private final static String PAYLOAD = "payload";
    private final static String ACTIONNAME = "action";

    private LambdaLogger logger;
    private ObjectMapper objectMapper;
    private Utils utils;
    private ApplicationContext ctx;

    public RequestRouter(ObjectMapper objectMapper, Utils utils) {
        this.objectMapper = objectMapper;
        this.utils = utils;
    }

    public void lambdaHandler(final InputStream request, final OutputStream response, final Context context)
            throws BadRequestException, InternalErrorException, JsonProcessingException {

        logger = context.getLogger();

        JsonNode inputObj;

        try {
            inputObj = objectMapper.readTree(IOUtils.toString(request));
        } catch (IOException e) {
            logger.log("Error while reading request\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage(), e);
        }

        if (inputObj == null || inputObj.get(PAYLOAD) == null || inputObj.get(PAYLOAD).toString().trim().equals("")) {
            logger.log("Invalid inputObj, could not find payload parameter");
            throw new BadRequestException("Could not find payload value in the request");
        }

        if (inputObj.get(ACTIONNAME) == null || inputObj.get(ACTIONNAME).asText().trim().equals("")) {
            logger.log("Invalid inputObj, could not find action parameter");
            throw new BadRequestException("Could not find action in the request");
        }

        final String actionClass = RequestRouter.class.getPackage().getName() + ".action." + inputObj.get(ACTIONNAME).asText() + "ApiAction";

        ApiAction action;
        try {
            Class<? extends ApiAction> classObject = Class.forName(actionClass).asSubclass(ApiAction.class);
            action = ctx.getBean(classObject);
        } catch (ClassNotFoundException e) {
            logger.log("Invalid action name");
            throw new BadRequestException("No such action");
        }

        final String output = action.handle(inputObj.toString(), context);

        try {
            utils.writeToStream(output, response);
        } catch (final IOException e) {
            logger.log("Error while writing response\n" + e.getMessage());
            throw new InternalErrorException(e.getMessage(), e);
        }
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public void setUtils(Utils utils) {
        this.utils = utils;
    }

    public void setCtx(ApplicationContext ctx) {
        this.ctx = ctx;
    }
}
