package de.hrs.sb.rebooking.configuration.service.application.configuration;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hrs.sb.rebooking.configuration.service.application.RequestRouter;
import de.hrs.sb.rebooking.configuration.service.application.Utils;
import de.hrs.sb.rebooking.configuration.service.application.action.CreateRbConfigApiAction;
import de.hrs.sb.rebooking.configuration.service.application.action.FindIdleApiAction;
import de.hrs.sb.rebooking.configuration.service.application.action.SaveRbConfigApiAction;
import de.hrs.sb.rebooking.configuration.service.application.model.DdbRbConfigDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Autowired private DdbRbConfigDao ddbRbConfigDao;

    @Bean
    public RequestRouter requestRouter() {
        return new RequestRouter(new ObjectMapper(), new Utils());
    }

    @Bean
    public SaveRbConfigApiAction saveRbConfigApiAction() {
        return new SaveRbConfigApiAction(new ObjectMapper(), ddbRbConfigDao);
    }

    @Bean
    public FindIdleApiAction findIdleApiAction () {
        return new FindIdleApiAction(new ObjectMapper(), ddbRbConfigDao);
    }

    @Bean
    public CreateRbConfigApiAction createRbConfigApiAction() {
        return new CreateRbConfigApiAction(new ObjectMapper(), ddbRbConfigDao);
    }

    @Configuration
    class DdbRbConfigDaoConfiguration {
        @Bean
        public DdbRbConfigDao ddbRbConfigDao() {
            DynamoDBMapperConfig.TableNameOverride tableNameOverride =
                    new DynamoDBMapperConfig.TableNameOverride("rb_rebooking_configuration");

            DynamoDBMapperConfig dynamoDBMapperConfig = new DynamoDBMapperConfig(tableNameOverride);
            AmazonDynamoDBClient client = new AmazonDynamoDBClient();
            client.setRegion(Region.getRegion(Regions.EU_WEST_1));
            DynamoDBMapper mapper = new DynamoDBMapper(client);

            return new DdbRbConfigDao(dynamoDBMapperConfig, mapper);
        }
    }
}
