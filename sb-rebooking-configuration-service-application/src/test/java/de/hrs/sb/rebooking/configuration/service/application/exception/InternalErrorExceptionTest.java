package de.hrs.sb.rebooking.configuration.service.application.exception;

import org.junit.Test;

public class InternalErrorExceptionTest {

    @Test(expected = InternalErrorException.class)
    public void throwsException() throws InternalErrorException {
        throw new InternalErrorException("Error");
    }

    @Test(expected = InternalErrorException.class)
    public void throwsExceptionWithException() throws InternalErrorException {
        throw new InternalErrorException("Error", new Exception("Another Error"));
    }
}
