package de.hrs.sb.rebooking.configuration.service.application.exception;

import org.junit.Test;

public class BadRequestExceptionTest {
    @Test(expected = BadRequestException.class)
    public void throwsException() throws BadRequestException {
        throw new BadRequestException("Error");
    }

    @Test(expected = BadRequestException.class)
    public void throwsExceptionWithException() throws BadRequestException {
        throw new BadRequestException("Error", new Exception("Another Error"));
    }
}
