package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import de.hrs.sb.rebooking.configuration.service.model.ProcIndicators;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import de.hrs.sb.rebooking.configuration.service.model.SaveRbConfigRequest;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class SaveRbConfigApiActionTest {

    private ObjectMapper objectMapper;
    private ObjectReader objectReader;
    private Context context;
    private RbConfigDao rbConfigDao;

    @Before
    public void setUp() {
        objectMapper = mock(ObjectMapper.class);
        objectReader = mock(ObjectReader.class);
        doReturn(objectReader).when(objectMapper).readerFor(SaveRbConfigRequest.class);

        context = mock(Context.class);

        LambdaLogger logger = mock(LambdaLogger.class);
        doReturn(logger).when(context).getLogger();

        rbConfigDao = mock(RbConfigDao.class);
    }

    @Test
    public void handleWorks()
            throws BadRequestException, InternalErrorException, IOException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("1234");

        SaveRbConfigRequest saveRbConfigRequest = mock(SaveRbConfigRequest.class);
        doReturn(rbConfig).when(saveRbConfigRequest).getPayload();

        doReturn(saveRbConfigRequest).when(objectReader).readValue(any(String.class));

        SaveRbConfigApiAction saveApiAction = new SaveRbConfigApiAction(objectMapper, rbConfigDao);
        saveApiAction.handle("", context);
    }

    @Test(expected = InternalErrorException.class)
    public void handleThrowsException()
            throws BadRequestException, InternalErrorException, IOException {

        doThrow(IOException.class).when(objectReader).readValue(any(String.class));

        SaveRbConfigApiAction saveApiAction = new SaveRbConfigApiAction(objectMapper, rbConfigDao);
        saveApiAction.handle("", context);
    }

    @Test(expected = BadRequestException.class)
    public void handleThrowsAnotherException()
            throws BadRequestException, InternalErrorException, IOException {

        doReturn(null).when(objectReader).readValue(any(String.class));

        SaveRbConfigApiAction saveApiAction = new SaveRbConfigApiAction(objectMapper, rbConfigDao);
        saveApiAction.handle("", context);
    }


    @Test(expected = BadRequestException.class)
    public void noRbConfigId()
            throws BadRequestException, InternalErrorException, IOException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("");

        SaveRbConfigRequest saveRbConfigRequest = mock(SaveRbConfigRequest.class);
        doReturn(rbConfig).when(saveRbConfigRequest).getPayload();

        doReturn(saveRbConfigRequest).when(objectReader).readValue(any(String.class));

        SaveRbConfigApiAction saveApiAction = new SaveRbConfigApiAction(objectMapper, rbConfigDao);
        saveApiAction.handle("", context);
    }


}
