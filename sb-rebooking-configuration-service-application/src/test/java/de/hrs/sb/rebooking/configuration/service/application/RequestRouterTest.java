package de.hrs.sb.rebooking.configuration.service.application;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hrs.sb.rebooking.configuration.service.application.action.ApiAction;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import static org.mockito.Mockito.*;

public class RequestRouterTest {
    private InputStream is;
    private OutputStream os;
    private Context ctx;
    private ApplicationContext appCtx;
    private RequestRouter requestRouter;
    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        is = IOUtils.toInputStream(
                //"{\"body\":{\"action\":\"CreateRbConfig\",\"body\":{\"reservationId\":\"123\",\"procIndicator\":\"idle\"}}}"
                "{ \"action\" : \"CreateRbConfig\",   \"payload\" : {     \"reservationId\" : \"1\",     \"procIndicator\" : \"idle\"   }}"
                //"{\"body\":{\"action\":\"FindIdle\",\"body\":{\"procIndicator\":\"idle\"}}}"
        );
        os = mock(OutputStream.class);
        ctx = mock(Context.class);

        LambdaLogger logger = mock(LambdaLogger.class);
        doReturn(logger).when(ctx).getLogger();

        ApplicationContext appCtx = mock(ApplicationContext.class);
        ApiAction action = mock(ApiAction.class);
        doReturn(action).when(appCtx).getBean(any(Class.class));

        requestRouter = new RequestRouter(
                new ObjectMapper(),
                new Utils()
        );
        requestRouter.setCtx(appCtx);

        objectMapper = mock(ObjectMapper.class);
    }

    @Test
    public void lambdaHandlerWorks() throws BadRequestException, InternalErrorException, JsonProcessingException {
        requestRouter.lambdaHandler(is, os, ctx);
    }

    @Test(expected = BadRequestException.class)
    public void classNotFound()
            throws BadRequestException, InternalErrorException, JsonProcessingException {

        is = IOUtils.toInputStream(
                "{ \"action\" : \"NoSuchAction\",   \"payload\" : {     \"reservationId\" : \"1\",     \"procIndicator\" : \"idle\"   }}"
        );
        requestRouter.lambdaHandler(is, os, ctx);
    }

    @Test(expected = InternalErrorException.class)
    public void errorReadingRequest() throws BadRequestException, InternalErrorException, IOException {
        doThrow(IOException.class).when(objectMapper).readTree(any(String.class));
        requestRouter.setObjectMapper(objectMapper);
        requestRouter.lambdaHandler(is, os, ctx);
    }

    @Test(expected = BadRequestException.class)
    public void invalidInput() throws IOException, BadRequestException, InternalErrorException {
        doReturn(null).when(objectMapper).readTree(any(String.class));

        requestRouter.setObjectMapper(objectMapper);
        requestRouter.lambdaHandler(is, os, ctx);
    }

    @Test(expected = BadRequestException.class)
    public void noPayloadError() throws IOException, BadRequestException, InternalErrorException {
        JsonNode inputObj = mock(JsonNode.class);
        doReturn(null).when(inputObj).get(any(String.class));
        doReturn(inputObj).when(objectMapper).readTree(any(String.class));

        requestRouter.setObjectMapper(objectMapper);
        requestRouter.lambdaHandler(is, os, ctx);
        verify(inputObj).get(any(String.class));
    }

    @Test(expected = BadRequestException.class)
    public void emptyPayloadError() throws IOException, BadRequestException, InternalErrorException {
        JsonNode inputObj = mock(JsonNode.class);
        JsonNode payload = mock(JsonNode.class);
        doReturn(payload).when(inputObj).get(any(String.class));
        doReturn("").when(payload).toString();
        doReturn(inputObj).when(objectMapper).readTree(any(String.class));

        requestRouter.setObjectMapper(objectMapper);
        requestRouter.lambdaHandler(is, os, ctx);
        verify(inputObj).get(any(String.class));
    }

    @Test(expected = BadRequestException.class)
    public void noActionNameError() throws IOException, BadRequestException, InternalErrorException {
        JsonNode jsonNode = mock(JsonNode.class);
        JsonNode payload = mock(JsonNode.class);

        doReturn(jsonNode).when(objectMapper).readTree(any(String.class));
        doReturn(payload).when(jsonNode).get("payload");
        doReturn(null).when(jsonNode).get("action");

        requestRouter.setObjectMapper(objectMapper);
        requestRouter.lambdaHandler(is, os, ctx);
    }

    @Test(expected = BadRequestException.class)
    public void emptyActionNameError() throws IOException, BadRequestException, InternalErrorException {
        JsonNode jsonNode = mock(JsonNode.class);
        JsonNode payload = mock(JsonNode.class);
        JsonNode action = mock(JsonNode.class);

        doReturn(jsonNode).when(objectMapper).readTree(any(String.class));
        doReturn(payload).when(jsonNode).get("payload");

        doReturn(action).when(jsonNode).get("action");
        doReturn("").when(action).asText();

        requestRouter.setObjectMapper(objectMapper);
        requestRouter.lambdaHandler(is, os, ctx);
    }

    @Test(expected = InternalErrorException.class)
    public void writeToStreamError() throws IOException, BadRequestException, InternalErrorException {
        Utils utils = mock(Utils.class);
        doThrow(IOException.class).when(utils).writeToStream(any(String.class), any(OutputStream.class));

        ApiAction apiAction = mock(ApiAction.class);
        doReturn("test").when(apiAction).handle(any(String.class), any(Context.class));

        requestRouter.setUtils(utils);
        requestRouter.lambdaHandler(is, os, ctx);
    }
}
