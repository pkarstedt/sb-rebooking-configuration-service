package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.DaoException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import de.hrs.sb.rebooking.configuration.service.model.FindIdleRequest;
import de.hrs.sb.rebooking.configuration.service.model.ProcIndicators;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class FindIdleApiActionTest {

    private ObjectMapper objectMapper;
    private ObjectReader objectReader;
    private Context context;
    private RbConfigDao rbConfigDao;

    @Before
    public void setUp() {
        objectMapper = mock(ObjectMapper.class);
        objectReader = mock(ObjectReader.class);
        doReturn(objectReader).when(objectMapper).readerFor(FindIdleRequest.class);

        context = mock(Context.class);

        LambdaLogger logger = mock(LambdaLogger.class);
        doReturn(logger).when(context).getLogger();

        rbConfigDao = mock(RbConfigDao.class);
    }

    @Test
    public void handleWorks()
            throws BadRequestException, InternalErrorException, DaoException, IOException {

        List<RbConfig> rbConfigList = new ArrayList<RbConfig>();
        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("1234");

        FindIdleRequest findIdleRequest = mock(FindIdleRequest.class);
        doReturn(rbConfig).when(findIdleRequest).getPayload();
        doReturn(findIdleRequest).when(objectReader).readValue(any(String.class));
        doReturn(rbConfigList).when(rbConfigDao).getIdle();

        FindIdleApiAction findByApiAction = new FindIdleApiAction(objectMapper, rbConfigDao);

        findByApiAction.handle(any(String.class), context);
    }

    @Test(expected = InternalErrorException.class)
    public void rbConfigDaoError()
            throws BadRequestException, InternalErrorException, DaoException, IOException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("1234");

        FindIdleRequest findIdleRequest = mock(FindIdleRequest.class);
        doReturn(rbConfig).when(findIdleRequest).getPayload();
        doReturn(findIdleRequest).when(objectReader).readValue(any(String.class));

        RbConfigDao rbConfigDao = mock(RbConfigDao.class);
        doThrow(Exception.class).when(rbConfigDao).getIdle();

        FindIdleApiAction findByApiAction = new FindIdleApiAction(objectMapper, rbConfigDao);

        findByApiAction.handle(any(String.class), context);
    }

    @Test(expected = BadRequestException.class)
    public void badRequest()
            throws BadRequestException, InternalErrorException, DaoException, IOException {

        doReturn(null).when(objectReader).readValue(any(String.class));

        FindIdleApiAction findByApiAction = new FindIdleApiAction(objectMapper, rbConfigDao);

        findByApiAction.handle(any(String.class), context);
    }


    @Test(expected = BadRequestException.class)
    public void noProcIndicator()
            throws BadRequestException, InternalErrorException, DaoException, IOException {

        RbConfig rbConfig = new RbConfig("","");
        FindIdleRequest findIdleRequest = mock(FindIdleRequest.class);
        doReturn(rbConfig).when(findIdleRequest).getPayload();
        doReturn(findIdleRequest).when(objectReader).readValue(any(String.class));

        FindIdleApiAction findByApiAction = new FindIdleApiAction(objectMapper, rbConfigDao);

        findByApiAction.handle(any(String.class), context);
    }

    @Test(expected = InternalErrorException.class)
    public void readerError()
            throws BadRequestException, InternalErrorException, DaoException, IOException {

        doThrow(IOException.class).when(objectReader).readValue(any(String.class));

        FindIdleApiAction findByApiAction = new FindIdleApiAction(objectMapper, rbConfigDao);

        findByApiAction.handle(any(String.class), context);
    }
}
