package de.hrs.sb.rebooking.configuration.service.application.exception;

import org.junit.Test;

public class DaoExceptionTest {

    @Test(expected = DaoException.class)
    public void throwsException() throws DaoException {
        throw new DaoException("Error");
    }

    @Test(expected = DaoException.class)
    public void throwsExceptionWithException() throws DaoException {
        throw new DaoException("Error", new Exception("Another Error"));
    }
}
