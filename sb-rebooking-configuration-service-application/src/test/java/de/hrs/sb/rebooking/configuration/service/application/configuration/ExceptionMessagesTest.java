package de.hrs.sb.rebooking.configuration.service.application.configuration;

import static org.junit.Assert.*;
import org.junit.Test;

public class ExceptionMessagesTest {

    @Test
    public void exceptionMessagesIsNotEmpty() {
        ExceptionMessages exceptionMessages = new ExceptionMessages();

        assertNotNull(ExceptionMessages.EX_DAO_ERROR);
        assertNotNull(ExceptionMessages.EX_INVALID_INPUT);
    }
}
