package de.hrs.sb.rebooking.configuration.service.application.action;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.DaoException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import de.hrs.sb.rebooking.configuration.service.application.model.RbConfigDao;
import de.hrs.sb.rebooking.configuration.service.model.CreateRbConfigRequest;
import de.hrs.sb.rebooking.configuration.service.model.ProcIndicators;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.mockito.Mockito.*;

public class CreateRbConfigApiActionTest {
    private ObjectMapper objectMapper;
    private ObjectReader objectReader;
    private Context context;
    private RbConfigDao rbConfigDao;

    @Before
    public void setUp() {
        objectMapper = mock(ObjectMapper.class);
        objectReader = mock(ObjectReader.class);
        doReturn(objectReader).when(objectMapper).readerFor(CreateRbConfigRequest.class);

        context = mock(Context.class);

        LambdaLogger logger = mock(LambdaLogger.class);
        doReturn(logger).when(context).getLogger();

        rbConfigDao = mock(RbConfigDao.class);
    }

    @Test
    public void handleWorks()
            throws BadRequestException, InternalErrorException, IOException, DaoException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("1234");

        CreateRbConfigRequest createRbConfigRequest = mock(CreateRbConfigRequest.class);
        doReturn(rbConfig).when(createRbConfigRequest).getPayload();
        doReturn(createRbConfigRequest).when(objectReader).readValue(any(String.class));

        doReturn("1234").when(rbConfigDao).createRbConfig(any(RbConfig.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }

    @Test(expected = InternalErrorException.class)
    public void readerFails()
            throws BadRequestException, InternalErrorException, IOException, DaoException {
        doThrow(IOException.class).when(objectReader).readValue(any(String.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }

    @Test(expected = BadRequestException.class)
    public void noPayload()
            throws BadRequestException, InternalErrorException, IOException, DaoException {

        doReturn(null).when(objectReader).readValue(any(String.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }

    @Test(expected = BadRequestException.class)
    public void wrongPayload()
            throws BadRequestException, InternalErrorException, IOException, DaoException {

        RbConfig rbConfig = new RbConfig("", ProcIndicators.IDLE);

        CreateRbConfigRequest createRbConfigRequest = mock(CreateRbConfigRequest.class);
        doReturn(rbConfig).when(createRbConfigRequest).getPayload();
        doReturn(createRbConfigRequest).when(objectReader).readValue(any(String.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }

    @Test(expected = InternalErrorException.class)
    public void daoDoesntWork()
            throws BadRequestException, InternalErrorException, IOException, DaoException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("1234");

        CreateRbConfigRequest createRbConfigRequest = mock(CreateRbConfigRequest.class);
        doReturn(rbConfig).when(createRbConfigRequest).getPayload();
        doReturn(createRbConfigRequest).when(objectReader).readValue(any(String.class));

        doThrow(DaoException.class).when(rbConfigDao).createRbConfig(any(RbConfig.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }

    @Test(expected = InternalErrorException.class)
    public void rbConfigIsEmpty()
            throws BadRequestException, InternalErrorException, IOException, DaoException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("1234");

        CreateRbConfigRequest createRbConfigRequest = mock(CreateRbConfigRequest.class);
        doReturn(rbConfig).when(createRbConfigRequest).getPayload();
        doReturn(createRbConfigRequest).when(objectReader).readValue(any(String.class));

        doReturn(null).when(rbConfigDao).createRbConfig(any(RbConfig.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }

    @Test(expected = InternalErrorException.class)
    public void rbConfigIsInvalid()
            throws BadRequestException, InternalErrorException, IOException, DaoException {

        RbConfig rbConfig = new RbConfig("1", ProcIndicators.IDLE);

        CreateRbConfigRequest createRbConfigRequest = mock(CreateRbConfigRequest.class);
        doReturn(rbConfig).when(createRbConfigRequest).getPayload();
        doReturn(createRbConfigRequest).when(objectReader).readValue(any(String.class));

        doReturn("").when(rbConfigDao).createRbConfig(any(RbConfig.class));

        CreateRbConfigApiAction createRbConfigApiAction = new CreateRbConfigApiAction(objectMapper, rbConfigDao);
        createRbConfigApiAction.handle("", context);
    }
}
