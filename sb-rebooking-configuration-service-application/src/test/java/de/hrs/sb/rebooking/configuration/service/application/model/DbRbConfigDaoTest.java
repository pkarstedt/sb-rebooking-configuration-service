package de.hrs.sb.rebooking.configuration.service.application.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import de.hrs.sb.rebooking.configuration.service.application.exception.DaoException;
import de.hrs.sb.rebooking.configuration.service.model.ProcIndicators;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import org.junit.Test;
import static org.mockito.Mockito.*;

public class DbRbConfigDaoTest {

    @Test
    public void createRbConfigWorks() throws DaoException {
        DynamoDBMapperConfig config = mock(DynamoDBMapperConfig.class);
        DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        RbConfig rbConfig = new RbConfig("123", ProcIndicators.IDLE);

        DdbRbConfigDao configDao = new DdbRbConfigDao(config, mapper);
        configDao.createRbConfig(rbConfig);
        verify(mapper).save(rbConfig, config);
    }

    @Test
    public void saveWorks() {
        DynamoDBMapperConfig config = mock(DynamoDBMapperConfig.class);
        DynamoDBMapper mapper = mock(DynamoDBMapper.class);

        RbConfig rbConfig = new RbConfig("123", ProcIndicators.IDLE);

        DdbRbConfigDao configDao = new DdbRbConfigDao(config, mapper);
        configDao.save(rbConfig);
        verify(mapper).save(rbConfig, config);
    }
}
