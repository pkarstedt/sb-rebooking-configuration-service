package de.hrs.sb.rebooking.configuration.service.application;

import com.amazonaws.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonProcessingException;
import de.hrs.sb.rebooking.configuration.service.application.exception.BadRequestException;
import de.hrs.sb.rebooking.configuration.service.application.exception.InternalErrorException;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.OutputStream;

import static org.mockito.Mockito.*;

public class ApplicationTest {

    @Test
    public void applicationTest()
            throws InternalErrorException, BadRequestException, JsonProcessingException {

        Application app = new Application();

        Application.handle(
                IOUtils.toInputStream(
                    "{ \"action\" : \"FindIdle\",   \"payload\" : {    \"procIndicator\" : \"idle\"   }}"
                ),
                mock(OutputStream.class),
                mock(Context.class)
        );
    }
}
