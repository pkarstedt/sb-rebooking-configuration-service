package de.hrs.sb.rebooking.configuration.service.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hrs.sb.rebooking.configuration.service.model.CreateRbConfigRequest;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import de.hrs.sb.rebooking.configuration.service.model.SaveRbConfigRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Component
public class RbConfigServiceClient {

    private RestTemplate restTemplate;

    private String url;

    public List<RbConfig> findIdle() throws IOException {
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        ObjectMapper objectMapper = new ObjectMapper();

        return Arrays.asList(
                objectMapper.readValue(response.getBody(), RbConfig[].class)
        );
    }

    public RbConfig create(RbConfig rbConfig) throws IOException {
        CreateRbConfigRequest rbConfigRequest = new CreateRbConfigRequest();
        rbConfigRequest.setPayload(rbConfig);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<CreateRbConfigRequest> request = new HttpEntity<>(rbConfigRequest, headers);

        ResponseEntity<RbConfig> result = restTemplate
                .exchange(url, HttpMethod.POST, request, RbConfig.class);


        return result.getBody();
    }

    public RbConfig save(RbConfig rbConfig) {
        SaveRbConfigRequest saveRbConfigRequest = new SaveRbConfigRequest();
        saveRbConfigRequest.setPayload(rbConfig);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<SaveRbConfigRequest> request = new HttpEntity<>(saveRbConfigRequest, headers);

        ResponseEntity<RbConfig> result = restTemplate.exchange(url, HttpMethod.POST, request, RbConfig.class);

        return result.getBody();
    }

    @Value("${rb.config.service.client.url}")
    public void setUrl(String url) {
        this.url = url;
    }

    @Autowired
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
}
