package de.hrs.sb.rebooking.configuration.service.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hrs.sb.rebooking.configuration.service.client.configuration.RbConfigServiceClientConfiguration;
import de.hrs.sb.rebooking.configuration.service.model.ProcIndicators;
import de.hrs.sb.rebooking.configuration.service.model.RbConfig;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RbConfigServiceClientTest {

    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    RbConfigServiceClient rbConfigServiceClient;

    private RbConfig rbConfig;

    private final String url = "https://1tqal11lw7.execute-api.eu-west-1.amazonaws.com/dev/rebooking";

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Before
    public void setUp() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        rbConfigServiceClient.setUrl(url);

        rbConfig = new RbConfig("1", ProcIndicators.IDLE);
        rbConfig.setRbConfigId("someid");

        ResponseEntity<String> findIdleresponseEntity = mock(ResponseEntity.class);
        ResponseEntity<String> responseEntity = mock(ResponseEntity.class);

        List<RbConfig> tConfigs = new ArrayList<>();
        tConfigs.add(new RbConfig("1", ProcIndicators.IDLE));

        doReturn(objectMapper.writeValueAsString(tConfigs)).when(findIdleresponseEntity).getBody();
        doReturn(findIdleresponseEntity).when(restTemplate).getForEntity(any(String.class), eq(String.class));

        doReturn(rbConfig).when(responseEntity).getBody();
        doReturn(responseEntity).when(restTemplate).exchange(any(String.class), eq(HttpMethod.POST), any(HttpEntity.class), eq(RbConfig.class));
    }

    @Test
    public void findIdleWorks() throws IOException {
        List<RbConfig> rbConfigs = rbConfigServiceClient.findIdle();

        assertFalse(rbConfigs.isEmpty());
        assertEquals(ProcIndicators.IDLE, rbConfigs.get(0).getProcIndicator());
        verify(restTemplate).getForEntity(url, String.class);
    }

    @Test
    public void createWorks() throws IOException {
        RbConfig response = rbConfigServiceClient.create(rbConfig);
        assertFalse(response.getRbConfigId().trim().equals(""));
    }

    @Test
    public void saveWorks() throws IOException {
        rbConfig.setProcIndicator(ProcIndicators.PROCESSING);
        RbConfig tRbConfig = rbConfigServiceClient.save(rbConfig);

        assertTrue(tRbConfig.getProcIndicator().equals(ProcIndicators.PROCESSING));
        verify(restTemplate).exchange(eq(url), eq(HttpMethod.POST), any(HttpEntity.class), eq(RbConfig.class));
    }

    @Test
    public void testIoC() {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(RbConfigServiceClientConfiguration.class);
        ctx.refresh();

        ctx.getBean(RestTemplate.class);
    }
}
