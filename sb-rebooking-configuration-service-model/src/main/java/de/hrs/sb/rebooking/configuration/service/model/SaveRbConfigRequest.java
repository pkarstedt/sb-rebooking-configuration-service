package de.hrs.sb.rebooking.configuration.service.model;

public class SaveRbConfigRequest extends AbstractApiRequest {
    public SaveRbConfigRequest() {
        this.action = "SaveRbConfig";
    }
}
