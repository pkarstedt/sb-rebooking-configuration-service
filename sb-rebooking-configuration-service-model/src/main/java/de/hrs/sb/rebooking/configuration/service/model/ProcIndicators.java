package de.hrs.sb.rebooking.configuration.service.model;

public class ProcIndicators {
    public static final String IDLE = "idle";
    public static final String PROCESSING = "processing";
}
