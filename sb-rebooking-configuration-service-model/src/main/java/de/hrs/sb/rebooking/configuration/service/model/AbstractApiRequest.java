package de.hrs.sb.rebooking.configuration.service.model;

public abstract class AbstractApiRequest {
    protected String action;
    private RbConfig payload;

    public String getAction() {
        return action;
    }

    public RbConfig getPayload() {
        return payload;
    }

    public void setPayload(RbConfig payload) {
        this.payload = payload;
    }
}
