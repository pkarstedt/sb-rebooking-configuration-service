package de.hrs.sb.rebooking.configuration.service.model;

public class CreateRbConfigRequest extends AbstractApiRequest {
    public CreateRbConfigRequest() {
        this.action = "CreateRbConfig";
    }
}
