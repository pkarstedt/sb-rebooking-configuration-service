package de.hrs.sb.rebooking.configuration.service.model;

public class FindIdleRequest extends AbstractApiRequest {
   public FindIdleRequest() {
      this.action = "FindIdleRbConfig";
   }
}
